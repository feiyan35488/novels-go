package main

import (
	"flag"
	"fmt"
)

var basedir string

// 定时抓取
func main() {

	var url string
	var port int
	flag.StringVar(&basedir, "dir", "/data/books", "the books dir")
	flag.StringVar(&url, "url", "/data/books.txt", "the book urls file")
	flag.IntVar(&port, "port", 8080, "the book urls file")
	flag.Parse()
	fmt.Println("url:", url, ",dir:", basedir)

	go fetchHtml(url)
	fmt.Println("starting server")
	startServer(port)
}
