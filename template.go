package main

import (
	"html/template"
	"bytes"
	"errors"
	"fmt"
)

var cache = make(map[string]*template.Template)

var tplIndex = `
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>首页</title>
        <link rel="stylesheet" type="text/css" href="/main.css">
    </head>
    <body>
        {{range $i1 , $book := .items}}
			{{with $book}}
            <div>
                <h3><a href="{{.url}}">{{.title}}</a></h3>
                <ul>
                 {{range $i2,$it := .items }}
						
                     <li>{{with $it}}<a href="{{.value}}">{{.key}}{{end}}</a></li>
                 {{end}}
                </ul>
            </div>
			{{end}}
        {{end}}
    </body>
<html>
`

var tplBook = `
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{.title}}</title>
        <link rel="stylesheet" type="text/css" href="/main.css">
    </head>
    <body>
        <ul>
            {{range $i ,$it := .items }}
                <li><a href="{{$it.value}}">{{$it.key}}</a></li>
            {{end}}
        </ul>
    </body>
<html>
`

var tplDetail = `
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{.title}}</title>
        <link rel="stylesheet" type="text/css" href="/main.css">
    </head>
    <body >
        <div>
        {{.content}}
        </div>
    </body>
<html>
`

var tplFrontDetail = `
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{.title}}</title>
        <link rel="stylesheet" type="text/css" href="/main.css">
    </head>
    <body>
        <h3>{{.title}}</h3>
        <div>
        {{.content}}
        </div>
        <div>
            {{if ne .prevId "" }}
            <a href="/{{.groupId}}/{{.bookId}}/{{.prevId}}.html">上一页</a>
            {{end}}
            <a href="/">首页</a>
            <a href="/{{.groupId}}/{{.bookId}}/index.html">目录</a>
            {{if ne .lastId "" }}
            <a href="/{{.groupId}}/{{.bookId}}/{{.lastId}}.html">下一页</a>
            {{end}}
        </div>
    </body>
<html>
`

var tplMainCss = `
body{
    font-size:1.25rem;
}
`

func init() {
	cache["index"], _ = template.New("index").Parse(tplIndex)
	cache["book"], _ = template.New("book").Parse(tplBook)
	cache["detail"], _ = template.New("detail").Parse(tplDetail)
	cache["frontDetail"], _ = template.New("frontDetail").Parse(tplFrontDetail)
}

func Render(name string, data map[string]interface{}) (result string, err interface{}) {
	defer func() {
		if err = recover(); err != nil {
			fmt.Println("template render error:", err)
		}
	}()

	w := &bytes.Buffer{}
	tpl, ok := cache[name]
	if ! ok {
		err = errors.New("没找到对应的模板")
		return
	}
	err = tpl.Execute(w, data)
	if err != nil {
		return
	}
	return w.String(), err
}
