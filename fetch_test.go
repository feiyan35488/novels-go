package main

import (
	"testing"
	"fmt"
	"strings"
	"github.com/PuerkitoBio/goquery"
)

func Test_Strings(t *testing.T) {
	fmt.Println(strings.Split("abababa", "a"))
	t.Log("success")
}

func Test_Map(t *testing.T) {
	m := make(map[string]string)
	m["a"] = "a"
	fmt.Println(m)
	updateMap(m)
	fmt.Println(m)

}

func updateMap(m map[string]string) {
	m["b"] = "b"
}

func Test_Index(t *testing.T) {
	bookArray := make([]map[string]interface{}, 1)
	item := make(map[string]interface{})
	bookArray[0] = item
	item["title"] = "植掌大唐"
	item["url"] = "/16/16567/index.html"
	items := make([]map[string]string, 1)
	item["items"] = items
	item1 := make(map[string]string)
	items[0] = item1
	item1["key"] = "第二百五十九章 因势利导"
	item1["value"] = "/16/16567/10345988.html"

	content, err := Render("index", map[string]interface{}{"items": bookArray})
	if err != nil {
		t.Error(err)
	} else {
		t.Log("success", content)
	}
}

func Test_Html(t *testing.T) {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(`<div><p>abcd</p></div>`))
	if err != nil {
		t.Error(err)
	}
	fmt.Println(doc.Find("div").Text())
	fmt.Println(doc.Find("div").Html())
}

func Test_Recover(t *testing.T) {

	fmt.Println("d")
	f()
	fmt.Println("e")
}
func f() {
	defer func() {
		fmt.Println("a")
		if err := recover(); err != nil {
			fmt.Println("b")
			fmt.Println(err)
		}
		fmt.Println("c")
	}()
	fmt.Println("f")
	panic(55)
	fmt.Println("h")
}
