package main

import (
	"testing"
	"fmt"
)

func Test_Render1(t *testing.T) {
	data := make(map[string]interface{})
	data["title"] = "this is title"
	data["content"] = "this is 内容"
	data["groupId"] = "1"
	data["bookId"] = "123"
	data["prevId"] = ""
	data["lastId"] = ""
	content, err := Render("frontDetail", data)
	if err == nil {
		fmt.Println(content)
		t.Log("通过")
	} else {
		t.Error("出错了", err)
	}

}
